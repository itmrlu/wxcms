package com.weixun.admin.controller;

import com.jfinal.json.JFinalJson;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.comm.controller.BaseController;
import com.weixun.model.User;
import com.weixun.admin.service.UserService;
//import com.weixun.admin.model.SysUser;
import com.weixun.utils.ajax.AjaxMsg;

import java.util.List;

/**
 * Created by myd on 2016/12/31.
 */

//@ControllerBind(controllerKey = "/user")
public class UserController extends BaseController {


    static UserService userService = new UserService();

    public void index(){}


    /**
     * 保存方法
     */
    public void save()
    {
//        getModel(User.class).save();
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            User user = getModel(User.class,"");
            if (user.getUserPk() != null && !user.getUserPk().equals(""))
            {
                //更新方法
                res = user.update();
            }
            else {
                //保存方法
                res = user.save();
            }
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("保存成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("保存失败");
            }
//        getBean(Staff.class).save();
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);
    }


    /**
     * 更新数据
     */
    public void update()
    {
//        getModel(User.class).update();
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            User user = getModel(User.class,"");
            if (user.getUserPk() != null && !user.getUserPk().equals(""))
            {
                //更新方法
                res = user.update();
            }
            else {
                //保存方法
                res = user.save();
            }
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("更新成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("更新失败");
            }
//        getBean(Staff.class).save();
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("更新失败");
        }
        renderJson(ajaxMsg);
    }

    /**
     * 用户登录
     */
    public void login(){
        String username = getPara("username");
        String password = getPara("password");
        List<Record> list= userService.find(username,password);
        renderJson(JFinalJson.getJson().toJson(list));
    }

    /**
     * 删除数据
     */
    public void delete()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        String user_pk = this.getPara("user_pk");
        int res =userService.deleteById(user_pk);
        if (res >0) {
            ajaxMsg.setState("success");
            ajaxMsg.setMsg("删除成功");
        }
        else
        {
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("删除失败");
        }

        renderJson(ajaxMsg);
    }

    /**
     * 获取列表
     */
    public void list(){
        List<Record> list= userService.findList();
        renderJson(JFinalJson.getJson().toJson(list));
    }

    /**
     * 基于layui的分页
     */
    public void pages(){
        int pageNumber = getParaToInt("page");
        int pageSize = getParaToInt("limit");
        String user_name = getPara("user_name");
        String user_loginname = getPara("user_loginname");
        String user_phone = getPara("user_phone");
        String user_email = getPara("user_email");
        Page<Record> userList = userService.paginate(pageNumber,pageSize,user_name,user_loginname,user_phone,user_email);//获得用户信息
        renderPageForLayUI(userList);
    }

    /**
     * 用户注册
     */
    public void register(){
        String username = getPara("username");
        String password = getPara("password");
        boolean result = userService.add(username,password);
        if(result)
            renderJson("10010");
        else
            renderJson("10011");
    }


    /**
     * jsp页面渲染
     */
    public void show(){
        renderJsp("user.jsp");
    }

    /**
     * 文件上传
     */
    public void image(){
        try{
            getFile(getPara("img"),"UTF-8");
            renderJson("20010");
        } catch (Exception e){
            renderJson("20012");
        }
    }
}
