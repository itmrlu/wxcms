package com.weixun.admin.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.admin.model.vo.TreeView;
import com.weixun.model.SysMenu;
import com.weixun.admin.model.vo.Menu;

import java.util.List;

public class MenuService {

    /**
     * 根据父菜单id获取子菜单
     * @param menu
     * @return
     */
    public List<SysMenu> findChildList(Menu menu)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from sys_menu where 1=1 ");
        if (menu.getId() != null)
        {
            stringBuffer.append("and menu_parent = "+menu.getId());
        }
        List<SysMenu> list = SysMenu.dao.find(stringBuffer.toString());
        return list;
    }

    public List<SysMenu> findRolesMenu(TreeView menu, String roles_pk)
    {

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select  m.* from sys_rolesmenu rm ");
        stringBuffer.append("left join sys_menu  m  on  rm.fk_menu_pk =m.menu_pk where 1=1 ");
        stringBuffer.append("and rm.checked='1' ");
        if (menu.getId() != null)
        {
            stringBuffer.append("and m.menu_parent = "+menu.getId());
        }
        if (menu.getType() != null && !menu.getType().equals(""))
        {
            stringBuffer.append("and m.menu_type = "+menu.getType());
        }
        if (roles_pk != null && !roles_pk.equals(""))
        {
            stringBuffer.append(" and rm.fk_roles_pk = "+roles_pk);
        }
        stringBuffer.append(" order by menu_number asc");

        List<SysMenu> list = SysMenu.dao.find(stringBuffer.toString());
        return list;
    }


    /**
     * 根据id获取信息
     * @param sysMenu
     * @return
     */
    public List<SysMenu> findList(SysMenu sysMenu)
    {
//        SysMenu sysMenu = SysMenu.dao.findById(id);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from sys_menu where 1=1 ");
        if (sysMenu.getMenuPk() != null && !sysMenu.getMenuPk().equals(""))
        {
            stringBuffer.append("and menu_pk = "+sysMenu.getMenuPk());
        }
        if (sysMenu.getMenuType() !=null && !sysMenu.getMenuType().equals(""))
        {
            stringBuffer.append("and menu_type = "+sysMenu.getMenuType());
        }
        List<SysMenu> list = SysMenu.dao.find(stringBuffer.toString());
        return list;
    }

    /**
     * 获取列表
     * @return
     */
    public List<Record> findList(String role_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("select a.*,0 cheched from sys_menu a where 1=1 ");
        if (role_pk != null && !role_pk.equals(""))
        {
            stringBuffer.append("and a.fk_role_pk = '"+role_pk+"'");
        }
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        return list;
    }

    /**
     * 获取列表
     * @return
     */
    public List<Record> findCheckedMenuList(String role_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append("select * from ( ");
        stringBuffer.append("select m.*,0 checked from sys_menu m ");
        stringBuffer.append("where m.menu_pk not in( ");
        stringBuffer.append("select m.menu_pk from sys_menu m ");
        stringBuffer.append("left join sys_rolesmenu rm on m.menu_pk = rm.fk_menu_pk ");
        stringBuffer.append("where 1 = 1 ");
        if (role_pk != null && !role_pk.equals(""))
        {
            stringBuffer.append("and rm.fk_roles_pk = '"+role_pk+"'");
        }
        stringBuffer.append(") ");
        stringBuffer.append(") a WHERE 1=1 ");
        stringBuffer.append("union all ");
        stringBuffer.append("select m.*, rm.checked from sys_menu m ");
        stringBuffer.append("left join sys_rolesmenu rm on m.menu_pk = rm.fk_menu_pk ");
        stringBuffer.append("where 1 = 1 ");
        if (role_pk != null && !role_pk.equals(""))
        {
            stringBuffer.append("and rm.fk_roles_pk = '"+role_pk+"'");
        }
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        return list;
    }
}
