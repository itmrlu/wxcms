package com.weixun.admin.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.model.SysLog;

import java.util.List;

/**
 * Created by myd on 2017/5/14.
 */
public class LogService {

    public List<SysLog> find()
    {
        String SQL = "SELECT * FROM sys_log";
        List<SysLog> list = SysLog.dao.find(SQL);
        return list;
    }

    public List<Record> findList()
    {
        String SQL = "SELECT * FROM sys_log";
        List<Record> list = Db.find(SQL);
        return list;
    }

}
