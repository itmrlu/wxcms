package com.weixun.utils.freemarker;

import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.*;
import java.util.Map;

public class FreemarkerUtils {

    /**
     * 生成静态页通用方法
     * @param data  页面上需要显示的数据
     * @param templatePath 模板路径
     * @param templateName 模板名称
     * @param targetHtmlPath 生成一面路径
     */
    public static void crateHTML(Map<String,Object> data, String templatePath, String templateName, String targetHtmlPath){
        Configuration freemarkerCfg = new Configuration(Configuration.VERSION_2_3_23);
        // 设置默认字符
        freemarkerCfg.setDefaultEncoding("UTF-8");
        //加载模版
        Writer out = null;

        try {
            //设置要解析的模板所在的目录，并加载模板文件
            freemarkerCfg.setDirectoryForTemplateLoading(new File(templatePath));
            //设置包装器，并将对象包装为数据模型
//            freemarkerCfg.setObjectWrapper(new DefaultObjectWrapper());
            //指定模版路径
            Template template = freemarkerCfg.getTemplate(templateName,"UTF-8");
            //静态页面路径
//            FileOutputStream fos = new FileOutputStream(targetHtmlPath);
//            out = new OutputStreamWriter(fos,"UTF-8");
            //设置输出文件
            File htmlFile = new File(targetHtmlPath);
            //判断目录是否存在，不存在则删除并创建
            if (!htmlFile.getParentFile().exists()) {
                htmlFile.getParentFile().mkdirs();
            }
            //页面存在则删除
            if (htmlFile.exists()) {
               htmlFile.delete();
            }
            htmlFile.createNewFile();
//            //设置输出流
//            out = new FileWriter(file);
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(htmlFile), "UTF-8"));
            //合并数据模型与模板
            template.process(data, out);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 删除文件
     * 删除实际的文章
     * @param targetHtmlPath
     */
    public static void deleteHtml(String targetHtmlPath)
    {
        File htmlFile = new File(targetHtmlPath);
        //页面存在则删除
        if (htmlFile.exists()) {
            htmlFile.delete();
        }
    }

}
