package com.weixun.utils.html;

import com.weixun.utils.StringUtils;

public class HtmlUtils {
    /**
     * 过滤掉html的标签，只保留内容
     * @param content
     * @return
     */
    public static String getNewContent(String content) {
        // TODO Auto-generated method stub
        int start = 0;
        int end = 0;
        try {
            while (content.indexOf("<style>") != -1 && content.indexOf("</style>") != -1) {
                content = content.replace(content.substring(content.indexOf("<style>"), content.indexOf("</style>") + 8), "");
            }
            while (content.indexOf("<STYLE>") != -1 && content.indexOf("</STYLE>") != -1) {
                content = content.replace(content.substring(content.indexOf("<STYLE>"), content.indexOf("</STYLE>") + 8), "");
            }
            if (content != null) {
                while (content.indexOf("<") != -1) {
                    start = content.indexOf("<");
                    end = content.indexOf(">");
                    if (start < end) {
                        while (content.substring(start + 1, end).contains("<")) {
                            start = content.indexOf("<", start + 1);
                        }
                        content = content.replace(content.substring(start, end + 1), "");
                    } else {
                        content = content.replace(content.substring(content.indexOf("<"), content.indexOf(">", content.indexOf("<")) + 1), "");
                    }
                }
            }
            content = content.trim().replace("&nbsp;", "").replace("<br>", "");
            content = StringUtils.truncateStr(content, 10000);
        }catch (Exception e)
        {
            e.getMessage();
        }
        return content;
    }
}
